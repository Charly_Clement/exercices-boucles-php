<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire une boucle do ... while permettant d'afficher la valeur de $x de 1 à 11 avec $x = 0; et while ($x <= 10);

?>

<!-- écrire le code après ce commentaire -->

<?php 
    $x = 0;

    do {
        $x++;
        echo "x contient la valeur de " . $x . "<br>";
    }while ($x <= 10);

?>
<!-- écrire le code avant ce commentaire -->

</body>
</html>