<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<h1>Mon premier mélange entre HTML et PHP</h1>

    <form method="GET">
        <input type="text" name="random" placeholder="tapez votre texte ici">
        <input type="submit" value="submit">construction<br><br>

        <h2>Ma liste déroulante</h2>
    </form><br>

<!-- écrire le code après ce commentaire -->

<?php
// Récuperer la valeur saisie dans le formulaire, compter et afficher le nombre de caractères 
// Avec une boucle, afficher une liste déroulante de la taille du nombre de caractères 
// ( Si mot de 10 caractères, faire une liste déroulante de 1 à 10)

// Garder la chaine de caractère en stock, supprimer lui toutes les voyelles et afficher la
// Si vous avez des apostrophes (') dans votre chaine, affichez la chaine avec les
// caractères échappés (\')

// Compter le nombre de caractères restant après la suppression des voyelles et faites la
// différence entre le nombre de caractères à l'entrée et à la sortie.

// Faites une condition en fonction du nombre de caractères : de 0 à 5 affiche :
// Pas beaucoup de voyelles. sinon : Belle suppression

// Bien sûr, afin de facilité la lecture de votre page, vous mettrez en forme avec de
// beaux blocs qui détermineront qui fait quoi ( exemple : <h2>Ma liste déroulante</h2>
// et la liste en dessous... )

// Afin de facilité la lecture des fichiers, utilisez la function includ

    $get = $_GET['random'];
    $count = strlen($get);

?>
    <select>
        <?php
            
            for ($x = 1; $x <$count; $x++) {
            
            echo "<option>" . $x . "</option>";
           
            }
        ?>
    </select>

<!-- écrire le code avant ce commentaire -->

</body>
</html>

